#ifndef DataModel_H
#define DataModel_H
#include "TObject.h"
#include "TClonesArray.h"

class CChannel : public TObject {
  public:
    CChannel() : TObject(), fPad(0), fRow(0), fSpectrumSize(0), fSpectrum(0x0) {} 

    Int_t fPad;
    Int_t fRow;
    Int_t fSpectrumSize;
    Int_t* fSpectrum; //[fSpectrumSize]

    ClassDef(CChannel, 1);
};

ClassImp(CChannel)

class CLaserEvent : public TObject {
  public:
  CLaserEvent() : TObject(), fNumberOfChannels(0), fChannel(new TClonesArray("CChannel")) {};

  Int_t fNumberOfChannels;
  TClonesArray* fChannel; //->
};
ClassImp(CLaserEvent)
#endif
