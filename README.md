# Python macro to analyse data for the lab course 'TPC' (experiment 22)

Launch on [Binder](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fwiechula%2Flab-course-tpc-v22/master?filepath=analysis.ipynb)
